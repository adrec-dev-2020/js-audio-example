/**
 * @type {HTMLInputElement}
 */
const $volume = document.querySelector('#volume');

/**
 * @type {HTMLInputElement}
 */
const $freq = document.querySelector('#freq');

/**
 * @type {HTMLSelectElement}
 */
const $type = document.querySelector('#type');

/**
 * @type {NodeListOf<Element>}
 */
const $pianoKeys = document.querySelectorAll('[data-freq]');

const audioContext = new AudioContext();

const gain = audioContext.createGain();
gain.connect(audioContext.destination);
gain.gain.value = 0.005;

const oscillator = audioContext.createOscillator();
oscillator.connect(gain);
oscillator.type = 'sine'; // "sawtooth" | "sine" | "square" | "triangle"
oscillator.frequency.value = 1000; // Choose sound freq

oscillator.start();

$volume.addEventListener('input', () => {
    gain.gain.value = $volume.value;
});

$freq.addEventListener('input', () => {
    oscillator.frequency.value = $freq.value;
});

$type.addEventListener('input', () => {
    oscillator.type = $type.value;
});

for (let $pianoKey of $pianoKeys) {
    $pianoKey.addEventListener('click', () => {
        oscillator.frequency.value = $pianoKey.getAttribute('data-freq');
    });
}
